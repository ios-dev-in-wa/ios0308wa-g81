//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {

	func ifElseExample() {
		move()

		if frontIsClear {
			turnLeft()
		} else {
			turnRight()
		}
	}

    //  Level name setup
    override func viewDidLoad() {
        levelName = "L555H" //  Level name
        super.viewDidLoad()
    }
	
	override func run() {
		whileLoopExample()
	}

	func whileLoopExample() {
		while noCandyPresent {
			if frontIsBlocked {
				break
			}
			put()
			move()
		}
		put()
		turnRight() // end of function indicator
		/*
		if frontIsClear {
			turnLeft()
		} else {
			turnRight()
		}
*/
	}

	func forLoopExample() {
		for _ in 0..<10 {
			move()
		}
	}

	func turnLeft() {
		turnRight()
		turnRight()
		turnRight()
	}

}
