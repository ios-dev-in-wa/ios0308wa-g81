//
//  GreenViewController.swift
//  G81L11
//
//  Created by Ivan Vasilevich on 9/14/20.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class GreenViewController: UIViewController {


	@IBOutlet var buttons: [UIButton]!
	@IBOutlet weak var whiteBox: UIView!

	@IBAction func buttonPressed(_ sender: UIButton) {
		print(sender.currentTitle ?? "")
		sender.isEnabled = false
		sender.superview?.backgroundColor = .black
		sender.removeFromSuperview()
	}

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		buttons.forEach { (button) in
			print(button.currentTitle ?? "")
			button.isEnabled = true
			let box = UIView()
			box.frame = button.frame
			box.frame.size.width -= 5
			box.frame.size.height -= 5
			box.center = button.center
			box.backgroundColor = .systemPink
			view.addSubview(box)
		}

		whiteBox.subviews.forEach {
			$0.backgroundColor = .green
		}

	}

}
