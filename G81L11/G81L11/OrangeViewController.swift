//
//  OrangeViewController.swift
//  G81L11
//
//  Created by Ivan Vasilevich on 9/14/20.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class OrangeViewController: UIViewController {

	@IBOutlet weak var finder: FaceView!
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		finder.faceColor = .systemBlue
    }

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		finder.happyLevel = 30
	}

}
