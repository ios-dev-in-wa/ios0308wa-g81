//
//  ViewController.swift
//  G81L3
//
//  Created by Ivan Vasilevich on 8/10/20.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
//		cascadeIfElse()
//		swichExample()
//		forLoopExample()
		foo()
//		drink()
//		bar()
	}

	func cascadeIfElse() {
		let speed = Int.random(in: 0...90)
		print("speed = \(speed)")

		if speed < 50 {
			print("normal speed")
		} else if speed < 60 && speed > 50 {
			print("warning")
		} else if speed > 60 {
			print("BUSTED")
		}
	}

	func swichExample() {
		let choise = Int.random(in: 0..<4)

		switch choise {
		case 0:
			print("Artem")
		case 1:
			print("Bohdan")
		case 2:
			print("Sergey")
		default:
			print("BANK")
		}
	}

	func forLoopExample() {
		let objectsCount = 4

		for x in 0..<objectsCount {
			for y in 0..<3 {
				print(x, y)
				//			print("hello student #\(x)")
			}
		}
	}


	func foo() { // takes data from user

		var age = 15
		while age < 19 {
			print("age =", age)
			bar(ageOfVisitor: age)
			age += 1
		}
		let a = multiplyByTen(number: 17)
		print("Artem", a)
		let i = multiplyByTen(number: 27)
		print("Ivan", i)
		let result = a + i
		print("Sum", result)

	}

	func bar(ageOfVisitor: Int) { // math

		if ageOfVisitor < 18 {
			let b = 10
			print("GoAway")
			print(b)
		} else {
			print("WELCOME")
		}
	}

	func multiplyByTen(number: Int) -> Int {
		let result = number * 10
		return result
	}

	func drink() {
//		a = 18
	}

}

