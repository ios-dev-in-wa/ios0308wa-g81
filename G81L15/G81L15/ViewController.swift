//
//  ViewController.swift
//  G81L15
//
//  Created by Ivan Vasilevich on 10/1/20.
//

import UIKit

class ViewController: UIViewController {

	typealias SimpleClosure = () -> ()

	var simpleClosure: SimpleClosure?

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		var numbers = [1, 4, 9, 3, 2]
		numbers.sort { (a, b) -> Bool in
			print(a, b)
			return a % 2 == 0
		}
		numbers.sort { ($0 % $1) == 0 }
		print(numbers)
		simpleClosure = { [weak self] in
			print("hello")
			print(self?.view.backgroundColor ?? "nil")
		}

		simpleClosure = { [unowned self] in
			print("hello")
			print(self.view.backgroundColor ?? "nil")
		}

	}

	private func showAlert(style: UIAlertController.Style) {
		let alert = UIAlertController(title: "Alert title", message: "I want to eat pizza", preferredStyle: style)

		let alertAction = UIAlertAction(title: "Ok", style: .default) { (_) in
			print("Pizza")
		}

		let alertCancellAction = UIAlertAction(title: "Cancel", style: .cancel)

		let alertDestructiveAction = UIAlertAction(title: "Do not eat Pizza", style: .destructive)
		alert.addAction(alertAction)
		alert.addAction(alertDestructiveAction)
		alert.addAction(alertCancellAction)

		present(alert, animated: true, completion: nil)

	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
//		showAlert(style: .alert)
	}

	deinit {
		print("Deinit of \(type(of: self))")
	}

}

