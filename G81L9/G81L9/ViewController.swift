//
//  ViewController.swift
//  G81L9
//
//  Created by Ivan Vasilevich on 9/7/20.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	let kCounter = "myCounterKey"

	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var counterLabel: UILabel!

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.

		view.backgroundColor = UIColor.init(hex: 0x35ab5a)
		counterLabel.text = numberFromUserDefaults.description
	}

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//		numberFromUserDefaults = numberFromUserDefaults + 1
		numberFromUserDefaults += 1
		counterLabel.text = numberFromUserDefaults.description
//		setNumberToUserDefaults(number: numberFromUserDefaults + 1)
		let image = UIImage(named: "contact")
		imageView.image = image
	}

//	func numberFromUserDefaults() -> Int {
//		return UserDefaults.standard.integer(forKey: kCounter)
//	}

//	func setNumberToUserDefaults(number: Int) {
//		UserDefaults.standard.set(number, forKey: kCounter)
//	}

	var numberFromUserDefaults: Int {
		get {
			return UserDefaults.standard.integer(forKey: kCounter)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: kCounter)
		}
	}


}

