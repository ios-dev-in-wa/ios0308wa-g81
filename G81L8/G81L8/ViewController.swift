//
//  ViewController.swift
//  G81L8
//
//  Created by Ivan Vasilevich on 9/3/20.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var greetingsLabel: UILabel!

	@IBAction func buttonPressed() {
		print("hello!")
		view.backgroundColor = UIColor.systemGreen
		greetingsLabel.text = "ПАУ!"
	}

}

