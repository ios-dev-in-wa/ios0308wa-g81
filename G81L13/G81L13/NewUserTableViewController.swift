//
//  NewUserTableViewController.swift
//  G81L13
//
//  Created by Ivan Vasilevich on 9/24/20.
//

import UIKit

class NewUserTableViewController: UITableViewController {

	@IBOutlet private weak var firstNameLabel: UITextField!

	override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

	@IBAction private func barButtonPressed(_ sender: UIBarButtonItem) {
		print(firstNameLabel.text)
	}

    // MARK: - Table view delegate
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		print(indexPath)
	}

}
