//
//  FirstViewController.swift
//  G81L10
//
//  Created by Ivan Vasilevich on 9/10/20.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

	let kCounter = "myCounterKey"

	var numberFromUserDefaults: Int {
		get {
			return UserDefaults.standard.integer(forKey: kCounter)
		}
		set {
			UserDefaults.standard.set(newValue, forKey: kCounter)
		}
	}

	@IBOutlet weak var counterLabel: UILabel!

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.

		view.backgroundColor = UIColor.lightGray
		counterLabel.text = numberFromUserDefaults.description
	}

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		//		numberFromUserDefaults = numberFromUserDefaults + 1
		numberFromUserDefaults += 1
		counterLabel.text = numberFromUserDefaults.description
		//		setNumberToUserDefaults(number: numberFromUserDefaults + 1)
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

		if let redVC = segue.destination as? ViewController {
			redVC.number = numberFromUserDefaults
		}


		print(segue.identifier ?? "no segue id")
	}

}

