//
//  ViewController.swift
//  G81L2
//
//  Created by Ivan Vasilevich on 8/5/20.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		block0z1()
		let age = 25
		print("my age = \(age)")
		//				age = 26
		
		var ageOfArtem = 17
		print("ageOfArtem = \(ageOfArtem) and b \(age)")
		ageOfArtem = 18
		print("ageOfArtem = \(ageOfArtem)")
		
		let pi = 3.14159
		//		+ - * / %
		ageOfArtem = ageOfArtem + Int(pi)
		//		ageOfArtem = -21
		//		+ -
		print("ageOfArtem = \(-ageOfArtem)")
		ageOfArtem = (ageOfArtem + 1)
		//		+= -= *= /= %=
		ageOfArtem += 1
		print("ageOfArtem = \(ageOfArtem)")
		//			a == 0,  b == 5 [0 - 5) [0 - 5]
		//		range 		a..<b 			a...b
		
		let randomNumber = Int.random(in: 0...1)
		let isLucker = randomNumber == 1
		//		== != < > <= >= -> Bool
		if isLucker {
			print("Lucker")
		} else {
			print("Looser")
		}
		// condition ? if true : else
		isLucker ? print("Lucker") : print("Looser")
//		let b = 44
//		let a = b > 10 ? 20 : 5
		
		
		let isRich = Int.random(in: 0...100) > 60
		let jail = true
		// and && / or || / !X
		if isRich || isLucker && !jail {
			print("life is good")
		} else {
			print("WASTED")
		}
	}
	
	func block0z1() {
		print("hello")
	}
	
	
}

