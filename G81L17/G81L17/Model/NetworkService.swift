//
//  NetworkService.swift
//  G81L16
//
//  Created by Ivan Vasilevich on 10/5/20.
//

import Foundation

protocol NetworkServiceProtocol {
	func getForecast(cityName: String, completion: @escaping (Forecast?, Error?) -> ())
}

class NetworkService: NetworkServiceProtocol {

	private struct Constants {
		struct APIDetails {
			static let APIScheme = "https"
			static let APIHost = "api.openweathermap.org"
			static let APIPath = "/data/2.5/weather"
		}
	}

	func getForecast(cityName: String, completion: @escaping (Forecast?, Error?) -> ()) {
		var components = URLComponents()
		components.scheme = Constants.APIDetails.APIScheme
		components.host   = Constants.APIDetails.APIHost
		components.path   = Constants.APIDetails.APIPath

//		https://api.openweathermap.org/data/2.5/weather?appid=43b9214baf299acfb5659fadadde5e82&units=metric
		// set URL parameters
		let parameters: [String: Any] = ["q" : "\(cityName)",
										 "appid" : "43b9214baf299acfb5659fadadde5e82",
										 "units" : "metric"]
		if !parameters.isEmpty {
			components.queryItems = [URLQueryItem]()
			for (key, value) in parameters {
				let queryItem = URLQueryItem(name: key, value: "\(value)")
				components.queryItems!.append(queryItem)
			}
		}
		guard let url = components.url else { return }
		var request = URLRequest(url: url)

		// set request http method
		request.httpMethod = "GET"

		// set Header parameters
//		request.setValue("Authorization", forHTTPHeaderField: "USER_TOKEN")

		URLSession.fetchData(type: Forecast.self, request: request, completion: completion)
	}
}

extension URLSession {
	static func fetchData<D: Decodable>(type: D.Type, request: URLRequest, completion: ((D?, Error?) -> Void)?) {
		URLSession.shared.dataTask(with: request) { data, response, error in
			if let data = data {
				do {
					let object = try JSONDecoder().decode(type, from: data)
					DispatchQueue.main.async {
						completion?(object, nil)
					}
				} catch let error {
					print(error)
					DispatchQueue.main.async {
						completion?(nil, error)
					}
				}
			}
		}.resume()
	}
}
