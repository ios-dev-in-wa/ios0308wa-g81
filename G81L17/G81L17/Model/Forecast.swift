//
//  Forecast.swift
//  G81L17
//
//  Created by Ivan Vasilevich on 10/15/20.
//

import Foundation

struct Forecast: Decodable {
	let name: String
	let main: Main
	let weather: [Weather]
}

struct Weather: Decodable {
	let icon: String
}

struct Main: Decodable {
	let temp: Double
}

extension Forecast: ForecastRenderable {
	var cityName: String {
		return name
	}
	var imageUrl: URL {
		return URL(string: "http://openweathermap.org/img/wn/\(self.weather.first!.icon)@2x.png")!
	}
	var temperature: Double {
		return main.temp
	}
}

protocol ForecastRenderable {
	var cityName: String { get }
	var imageUrl: URL { get }
	var temperature: Double { get }
}
