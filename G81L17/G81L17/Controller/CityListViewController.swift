//
//  CityListViewController.swift
//  G81L17
//
//  Created by Ivan Vasilevich on 10/8/20.
//

import UIKit

class CityListViewController: UIViewController, UITableViewDataSource {

	// MARK: - @IBOutlets

	@IBOutlet private weak var tableView: UITableView!

	var students = ["Artem",
					"ViktorH",
					"ViktorK",
					"Serhii",
					"Pavel",
					"Bohdan",
					"Ivan"]

	// MARK: - Lifecycle

	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
		tableView.dataSource = self
		tableView.delegate = self
	}

	// MARK: - UITableViewDataSource

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return students.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath)
		cell.textLabel?.text = students[indexPath.row]
		let lp = UILongPressGestureRecognizer(target: self, action: #selector(blackMagick(sender:)))
		cell.addGestureRecognizer(lp)
		return cell
	}

	@objc func blackMagick(sender: UILongPressGestureRecognizer) {
		guard let cell = sender.view as? UITableViewCell, let indexPath = tableView.indexPath(for: cell), sender.state == .began  else { return }
		print(indexPath)
	}

}

extension CityListViewController: UITableViewDelegate {

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let object = students[indexPath.row]
		let vc = ForecastViewController()
		vc.cityName = object
		navigationController?.pushViewController(vc, animated: true)

	}

	func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
		return "\(students.count) totatal students in list"
	}

}

extension CityListViewController: CityReceiver {



	func didAddStudent(name: String) {
		students.append(name)
		tableView.reloadData()
	}

}

protocol CityReceiver: class {
	func didAddStudent(name: String)
}
