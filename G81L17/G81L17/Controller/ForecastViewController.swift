//
//  ForecastViewController.swift
//  G81L17
//
//  Created by Ivan Vasilevich on 10/8/20.
//

import UIKit
import SDWebImage


class ForecastViewController: UIViewController {
	var delegate: (ForecastRenderable & NetworkService)?
	let networkManager = NetworkService()
	var cityName: String = ""

	@IBOutlet weak var imageVuew: UIImageView!
	@IBOutlet weak var cityNameLabel: UILabel!
	@IBOutlet weak var temperatureLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
		print(cityName)

		networkManager.getForecast(cityName: cityName) { (forecast, error) in
			if let forecast = forecast {
				self.showForecast(data: forecast)
			} else {
				print(error)
			}
		}

    }

	private func showForecast(data: ForecastRenderable) {
		cityNameLabel.text = data.cityName
		temperatureLabel.text = data.temperature.description + "℃"
		imageVuew.sd_setImage(with: data.imageUrl)
	}


}
