//
//  Car.swift
//  G81L7
//
//  Created by Ivan Vasilevich on 8/31/20.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import Foundation

class Vehicle: NSObject {

	var seatsCount: Int
	var freeSeats: Int

	func getSeat() {
		print("Vehicle foo")
		freeSeats -= 1
	}

	init(seatsCount: Int) {
		self.seatsCount = seatsCount
		freeSeats = seatsCount
	}

}

class Car: Vehicle {

	var color: String
	private(set) var volume: Double

	init(bodyColor: String, seatsCount: Int = 5) {
		self.color = bodyColor
		self.volume = 15
		super.init(seatsCount: seatsCount)
	}

	override func getSeat() {
		super.getSeat()
		checkSafty()
		print("Car foo")
	}

	private func checkSafty() {
		print("piu piu piu")
	}


}
