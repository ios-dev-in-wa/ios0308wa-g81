//
//  ViewController.swift
//  G81L6
//
//  Created by Ivan Vasilevich on 8/20/20.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		//		testOptionalTypy()
		//		workWithArrays()
		workWithDictionaries()
	}

	func testOptionalTypy() {
		let userInput = "a5"
		let multiplyer = 2

		let numberFromUser = Int(userInput) // Int / nil

		//		let realNumberFromUser = numberFromUser! // Force unwrap

		//		let realNumberFromUser = numberFromUser ?? 2 // Default value

		if let realNumberFromUser = numberFromUser {// Optional binding
			let result = realNumberFromUser * multiplyer
			print(result)
		}

		guard let realNumberFromUser = numberFromUser else {
			print("input correct number")
			return
		}

		let result = realNumberFromUser * multiplyer
		print(result)

	}

	func workWithArrays() {
		var numbers = [20, 7, 55]
		print(numbers.count) // 3
		print(numbers[1]) // 7
		numbers[1] = 8
		print(numbers)
		numbers.insert(9, at: 1)
		print(numbers)
		//		print(numbers.first, numbers.last) // Optional(20) Optional(55)

		for i in 0..<numbers.count {
			print(numbers[i])
		}
	}

	func workWithDictionaries() {
		let phoneBook = ["Fire" : "101",
						 "Police" : "102",
						 "Ambulance" : "103",
						 "Gaz" : "104"]
		print(phoneBook["Police"])
		print(phoneBook["Polic"])

		for (key, value) in phoneBook {
			print("C:\(key)\t\tT:\(value)")
		}

		chooseYourDestiny(dirrection: Dirrection.forward)
	}

	enum Dirrection: String {
		case left = "wife"
		case right = "xcode"
		case forward = "death"
	}

	func chooseYourDestiny(dirrection: Int) {
		switch dirrection {
		case 0: // left
			print("wife")
		case 1: // forward
			print("xcode")
		case 2: // right
			print("death")
		default:
			print("unsuported dirrection")
		}
	}

	func chooseYourDestiny(dirrection: Dirrection) {
		print(dirrection.rawValue)
//		switch dirrection {
//		case .left:
//			print("wife")
//		case .forward:
//			print("xcode")
//		case .right:
//			print("death")
////		default:
////			print("unsuported dirrection")
//		}
	}

}

