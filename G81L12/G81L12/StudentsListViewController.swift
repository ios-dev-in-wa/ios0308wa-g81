//
//  StudentsListViewController.swift
//  G81L12
//
//  Created by Ivan Vasilevich on 9/17/20.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class StudentsListViewController: UIViewController, UITableViewDataSource {

	// MARK: - @IBOutlets

	@IBOutlet private weak var tableView: UITableView!

	var a: Int = 1

	var students = ["Artem",
					"ViktorH",
					"ViktorK",
					"Serhii",
					"Pavel",
					"Bohdan",
					"Ivan"]

	// MARK: - Lifecycle

	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
		tableView.dataSource = self
		tableView.delegate = self
	}

	// MARK: - UITableViewDataSource

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return students.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath)
		cell.textLabel?.text = students[indexPath.row]
		let lp = UILongPressGestureRecognizer(target: self, action: #selector(blackMagick(sender:)))
		cell.addGestureRecognizer(lp)
		return cell
	}

	@objc func blackMagick(sender: UILongPressGestureRecognizer) {
		guard let cell = sender.view as? UITableViewCell, let indexPath = tableView.indexPath(for: cell), sender.state == .began  else { return }
		print(indexPath)
	}

}

extension StudentsListViewController: UITableViewDelegate {

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let object = students[indexPath.row]
		let vc = StudentViewController()
		vc.studentToDisplay = object
//		vc.delegate = self
//		present(vc, animated: true, completion: nil)
		vc.completion = { (str) in
			self.students.append(str)
			self.tableView.reloadData()
		}
		navigationController?.pushViewController(vc, animated: true)
	}

	func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
		return "\(students.count) totatal students in list"
	}

}

extension StudentsListViewController: StudentReceiver {



	func didAddStudent(name: String) {
		students.append(name)
		tableView.reloadData()
	}

}
