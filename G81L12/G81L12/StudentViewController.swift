//
//  StudentViewController.swift
//  G81L12
//
//  Created by Ivan Vasilevich on 9/24/20.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

protocol Foo {
	func foo()
}

protocol StudentReceiver: class {
	func didAddStudent(name: String)
	var a: Int { get set }
}

protocol ArrayStudentReceiver: StudentReceiver, Foo {
	func didAddStudent(names: [String])
}

//struct A: StudentReceiver {
//	func didAddStudent(name: String) {
//		print(name)
//	}
//}

typealias AddStudentBlock = (String) -> Void

class StudentViewController: UIViewController {

	@IBOutlet private weak var textField: UITextField!
	@IBOutlet private weak var nameLabel: UILabel!

	weak var delegate: StudentReceiver?

	var completion: AddStudentBlock?

	var studentToDisplay: String = ""

	override func viewDidLoad() {
        super.viewDidLoad()

		setupView()
    }

	private func setupView() {
		nameLabel.text = studentToDisplay
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		guard let name = textField.text, name.count > 2 else {
			return
		}
//		delegate?.didAddStudent(name: name)
		completion?(name)
		delegate?.a = 3
		print(delegate?.a ?? 32)
	}

}
