//
//  ViewController.swift
//  G81L12
//
//  Created by Ivan Vasilevich on 9/17/20.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var redView: UIView!

	private var isOn: Bool = false {
		didSet {
			redView.backgroundColor = isOn ? .systemGreen : .systemIndigo
		}
	}

	@IBAction func tapRecognized(_ sender: UITapGestureRecognizer) {
		isOn = !isOn
	}

	@IBAction func panRecognized(_ sender: UIPanGestureRecognizer) {
		let position = sender.location(in: view)
		redView.center = position
		if isOn {
			makeCopy(of: sender.view!)
		}
	}

	private func makeCopy(of view: UIView, with color: UIColor = .random) {
		let copy = UIView(frame: view.frame)
		copy.backgroundColor = color
		self.view.addSubview(copy)
	}


}

extension ViewController: UITableViewDelegate {

}

extension UIColor {
	class var random: UIColor {
		return UIColor(
			red: CGFloat.random(in: 0...1),
			green: CGFloat.random(in: 0...1),
			blue: CGFloat.random(in: 0...1),
			alpha: CGFloat.random(in: 0...1))
	}
}
