//
//  FileManager+Extensions.swift
//  G81L16
//
//  Created by Ivan Vasilevich on 10/5/20.
//

import Foundation

extension FileManager {
	static var documentsDir: String {
		var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [String]
		return paths[0]
	}

	class func cachesDir() -> String {
		var paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [String]
		return paths[0]
	}
}
