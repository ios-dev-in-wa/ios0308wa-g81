//
//  Photo.swift
//  G81L16
//
//  Created by Ivan Vasilevich on 10/5/20.
//

import Foundation

struct Photo: Codable {
	let albumId: Int
	let id: Int
	let title: String
	let url: String
	let thumbnailUrl: String
}
