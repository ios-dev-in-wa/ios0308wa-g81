//
//  ViewController.swift
//  G81L16
//
//  Created by Ivan Vasilevich on 10/5/20.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var myLabel: UILabel!
	override func viewDidLoad() {
		super.viewDidLoad()

		let data = "// Do any additional setup after loading the view.".data(using: .utf8)
		let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
		let documentsDirectory = paths[0].appendingPathComponent("file.txt")

		print(documentsDirectory)
		try? data?.write(to: documentsDirectory)
	}


	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let point = touches.first!.location(in: view)

		UIView.transition(with: myLabel, duration: 2, options: .transitionFlipFromTop) {
			self.myLabel.text = Date().description
			self.myLabel.center = point
		}

//		UIView.animate(withDuration: 4, delay: 0, options: [.beginFromCurrentState, .transitionCurlUp]) {
//
//		}
	}


}
