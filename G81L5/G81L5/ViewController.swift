//
//  ViewController.swift
//  G81L5
//
//  Created by Ivan Vasilevich on 8/17/20.
//  Copyright © 2020 ivan.besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		workWithString()
		foo(3)
	}

	func workWithString() {//1...2
		let authorName = "iv" + " " + "an"
		let inputText = """
//  Created by Ivan Vasilevich on 8/17/20.
//  Copyright © 2020 Ivan.besarab. All rights reserved.
"""
		if inputText.lowercased().contains(authorName) {
			print("\(authorName) wrote this program")
		}
		else {
			print("\(authorName) wasn't make this program")
		}

		var name = "Ivan"
		name.insert("B", ind: 2)
		print(name)

		let string = name
		let nsstring = NSString(string: string)

	}

	func foo(_ a: Int) {
		print(a)
	}

}

extension String {
	mutating func insert(_ string: String, ind: Int) {
		self.insert(contentsOf: string, at:self.index(self.startIndex, offsetBy: ind) )
	}
}
